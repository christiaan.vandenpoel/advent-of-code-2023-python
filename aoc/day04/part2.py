# Advent of Code - Day 4 - Part Two
import re

def result(lines):
    result = 0

    scratchcards = list()

    for line in lines:
        p = line.split(": ")
        card_id = int(re.split("\s+", p[0])[1])

        winning_numbers_string, my_numbers_string = p[1].split(" | ")
        winning_numbers = set()
        my_numbers = set()

        for winning_number_substr in winning_numbers_string.split(" "):
            if winning_number_substr == "":
                continue
            winning_numbers.add(int(winning_number_substr.strip()))

        for my_numbers_substr in my_numbers_string.split(" "):
            if my_numbers_substr == "":
                continue
            my_numbers.add(int(my_numbers_substr.strip()))

        scratchcards.append((card_id, winning_numbers, my_numbers, 1))

    for idx, card in enumerate(scratchcards):
        card_id = card[0]
        winning_numbers = card[1]
        my_numbers = card[2]
        copies = card[3]
        l = len(winning_numbers.intersection(my_numbers))
        if l >= 1:
            for next_card_id in range(card_id, card_id + l):
                cid, wn, my, count = scratchcards[next_card_id]
                scratchcards[next_card_id] = (cid, wn, my, (count + copies))

    for card in scratchcards:
        result += card[3]

    return result
