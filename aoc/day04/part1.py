# Advent of Code - Day 4 - Part One
import re

def result(lines):
    result = 0

    scratchcards = list()

    for line in lines:
        p = line.split(": ")
        card_id = int(re.split("\s+", p[0])[1])

        winning_numbers_string, my_numbers_string = p[1].split(" | ")
        winning_numbers = set()
        my_numbers = set()

        for winning_number_substr in winning_numbers_string.split(" "):
            if winning_number_substr == "":
                continue
            winning_numbers.add(int(winning_number_substr.strip()))

        for my_numbers_substr in my_numbers_string.split(" "):
            if my_numbers_substr == "":
                continue
            my_numbers.add(int(my_numbers_substr.strip()))

        scratchcards.append((card_id, winning_numbers, my_numbers))

    for card in scratchcards:
        l = len(card[1].intersection(card[2]))
        if l >= 1:
            result += 2 ** (l - 1)

    return result
