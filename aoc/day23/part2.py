# Advent of Code - Day 23 - Part Two
def result(lines):
    return 0
    # takes way too long
    width = len(lines[0])
    height = len(lines)
    start = (1,0)
    end = (width - 2, height-1)

    slopes = ['<','^','>','v']
    directions = [(-1,0),(0,-1),(1,0),(0,1)]
    chart = dict()
    
    for y, line in enumerate(lines):
        for x, char in enumerate(line):
            if char not in slopes:
                chart[(x,y)] = char
            else:
                chart[(x,y)] = '.'

    queue = [(0, start, [start])]

    paths = []

    while queue:
        new_queue = []
        # print([(s,p) for s,p,_ in queue])
        for steps, position, path in queue:
            if position == end:
                paths.append((steps, position, path))
                continue
            # visited.add(position)
            x,y = position
            for direction, (dx, dy) in enumerate(directions):
                # direction:
                # 0: left   west    <
                # 1: up     north   ^
                # 2: right  east    >
                # 3: down   south   v
                new_position = (x+dx,y+dy)
                # if debug:
                #     print("new position: {0}".format(new_position))
                if new_position not in chart:
                    continue
                if new_position in path:
                    continue
                if chart[new_position] == '#':
                    continue
                if chart[new_position] not in ([slopes[direction], '.']):
                    continue
                new_queue.append((steps+1, new_position, path + [new_position]))

        queue = new_queue

    return max([steps for steps, _, _ in paths])
