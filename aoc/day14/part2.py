# Advent of Code - Day 14 - Part Two
def tilt_north(lines, chart):
    for y in range(1, len(lines)):
        for x in range(0, len(lines[0])):
            char = chart[(x,y)]
            if char != 'O':
                continue
            offset = 1
            while True:
                north_coordinate = (x,y-offset)
                if north_coordinate[1] == -1 or chart[north_coordinate] == '#' or chart[north_coordinate] == 'O':
                    chart[(x,y)] = '.'
                    chart[(x,y-offset+1)] = 'O'
                    break

                offset += 1

    return sum([len(lines) - y for y in range(0, len(lines)) for x in range(0, len(lines[0])) if chart[(x,y)] == 'O'])    

def tilt_south(lines, chart):
    for y in range(len(lines)-1,-1,-1):
        for x in range(0, len(lines[0])):
            char = chart[(x,y)]
            if char != 'O':
                continue
            offset = 1
            while True:
                south_coordinate = (x,y+offset)
                if south_coordinate[1] == len(lines) or chart[south_coordinate] == '#' or chart[south_coordinate] == 'O':
                    chart[(x,y)] = '.'
                    chart[(x,y+offset-1)] = 'O'
                    break

                offset += 1
    
    return sum([len(lines) - y for y in range(0, len(lines)) for x in range(0, len(lines[0])) if chart[(x,y)] == 'O'])    

def tilt_west(lines, chart):
    width = len(lines[0])
    height = len(lines)

    for x in range(1,width):
        for y in range(height):
            char = chart[(x,y)]
            if char != 'O':
                continue
            offset = 1
            while True:
                west_coordinate = (x-offset,y)
                if west_coordinate[0] == -1 or chart[west_coordinate] == '#' or chart[west_coordinate] == 'O':
                    chart[(x,y)] = '.'
                    chart[(x-offset+1,y)] = 'O'
                    break

                offset += 1

    return sum([len(lines) - y for y in range(0, len(lines)) for x in range(0, len(lines[0])) if chart[(x,y)] == 'O'])    

def tilt_east(lines, chart):
    width = len(lines[0])
    height = len(lines)

    for x in range(width-1,-1,-1):
        for y in range(height):
            char = chart[(x,y)]
            if char != 'O':
                continue
            offset = 1
            while True:
                east_coordinate = (x+offset,y)
                if east_coordinate[0] == width or chart[east_coordinate] == '#' or chart[east_coordinate] == 'O':
                    chart[(x,y)] = '.'
                    chart[(x+offset-1,y)] = 'O'
                    break

                offset += 1

    return sum([len(lines) - y for y in range(0, len(lines)) for x in range(0, len(lines[0])) if chart[(x,y)] == 'O'])    

def result(lines):
    chart = dict()

    for y,line in enumerate(lines):
        for x,char in enumerate(line):
            chart[(x,y)] = char

    result = 0
    # Duh, 1000 cycles gives the same value as 1_000_000_000
    for _ in range(1_000):
        tilt_north(lines, chart)
        tilt_west(lines, chart)
        tilt_south(lines, chart)
        result = tilt_east(lines, chart)

    return result