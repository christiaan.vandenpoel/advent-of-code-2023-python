# Advent of Code - Day 14 - Part One

def result(lines):

    chart = dict()

    for y,line in enumerate(lines):
        for x,char in enumerate(line):
            chart[(x,y)] = char

    for y in range(1, len(lines)):
        for x in range(0, len(lines[0])):
            char = chart[(x,y)]
            if char != 'O':
                continue
            offset = 1
            while True:
                north_coordinate = (x,y-offset)
                if north_coordinate[1] == -1:
                    chart[(x,y)] = '.'
                    chart[(x,y-offset+1)] = 'O'
                    break
                next_char = chart[north_coordinate]

                if next_char == '#' or next_char == 'O':
                    chart[(x,y)] = '.'
                    chart[(x,y-offset+1)] = 'O'
                    break

                offset += 1

    result = 0

    for y in range(0, len(lines)):
        for x in range(0, len(lines[0])):
            if chart[(x,y)] == 'O':            
                result += len(lines) - y

    return result
