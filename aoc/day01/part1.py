# Advent of Code - Day 1 - Part One

def result(lines):
    total = 0
    for line in lines:
        number = ""
        for char in line:
            if ord(char) in range(ord("0"),ord("9")+1):
                number += char
                break
        for char in line[::-1]:
            if ord(char) in range(ord("0"),ord("9")+1):
                number += char
                break
        total += int(number)
    return total
