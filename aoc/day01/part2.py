# Advent of Code - Day 1 - Part Two

string_to_int_map = {
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9
}

def find_first_integer(line, pos_lamdba):
    l = len(line)
    for idx in range(l):
        pos = pos_lamdba(idx)
        char = line[pos]
        if ord(char) in range(ord("0"),ord("9")+1):
            return int(char)

        for k,v in string_to_int_map.items():
            if line[pos:].startswith(k):
                return v


def result(lines):
    total = 0
    for line in lines:
        first_digit = find_first_integer(line, lambda idx: idx)
        second_digit = find_first_integer(line, lambda idx: len(line)-idx-1)
        total += first_digit * 10 + second_digit
    return total
