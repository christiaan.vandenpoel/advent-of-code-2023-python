direction_switcher = {
    'north': {
        '|': 'north', 
        '7': 'west', 
        'F': 'east'
    },
    'west':  {
        '-': 'west', 
        'L': 'north', 
        'F': 'south' 
    },
    'south': {
        '|': 'south', 
        'L': 'east', 
        'J': 'west'
    },
    'east':  {
        '-': 'east', 
        '7': 'south', 
        'J': 'north'
    }
}

def parse_chart(lines):
    chart = dict()
    start_position = None
    for y, line in enumerate(lines):
        for x, char in enumerate(line):
            chart[(x,y)] = char
            if char == 'S':
                start_position = (x,y)
    return chart, start_position    

def coordinate_into_direction(coordinate, direction):
    x,y = coordinate
    if direction == 'north':
        return (x,y-1)
    if direction == "west":
        return (x-1,y)
    if direction == 'south':
        return(x,y+1)
    if direction == 'east':
        return(x+1,y)