# Advent of Code - Day 10 - Part Two
from aoc.day10.utils import direction_switcher, parse_chart, coordinate_into_direction

def result(lines):
    chart,start_position = parse_chart(lines)
    
    start_direction = None
    for direction in ['north', 'west', 'south', 'east']:
        next_coordinate = coordinate_into_direction(start_position, direction)
        if next_coordinate not in chart:
            continue
        pipe = chart[next_coordinate]
        if pipe in direction_switcher[direction].keys():
            start_direction = direction
            break
