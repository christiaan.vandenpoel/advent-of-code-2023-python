# Advent of Code - Day 10 - Part One
from aoc.day10.utils import direction_switcher, parse_chart, coordinate_into_direction

def result(lines):
    chart,start_position = parse_chart(lines)
    
    start_direction = None
    for direction in ['north', 'west', 'south', 'east']:
        next_coordinate = coordinate_into_direction(start_position, direction)
        if next_coordinate not in chart:
            continue
        pipe = chart[next_coordinate]
        if pipe in direction_switcher[direction].keys():
            start_direction = direction
            break

    step_count = 0

    current_direction = start_direction
    current_location = start_position    

    while True:
        current_location = coordinate_into_direction(current_location, current_direction)
        step_count += 1
        if current_location == start_position:
            break;
        current_direction = direction_switcher[current_direction][chart[current_location]]

    return step_count // 2



