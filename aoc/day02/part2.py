# Advent of Code - Day 2 - Part Two
from aoc.day02.game import parse_game_info
import functools
from collections import defaultdict

def result(lines):
    result = 0

    game_info = parse_game_info(lines)

    for _, tests in game_info.items():
        set = defaultdict(int)
        for test in tests:
            for color, count in test.items():
                set[color] = max(set[color],count)

        power = functools.reduce(lambda a,b: a*b, set.values())
        result += power

    return result