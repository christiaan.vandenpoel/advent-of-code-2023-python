from collections import defaultdict

def parse_game_info(lines):
    result = defaultdict(list)

    for line in lines:
        game_info, tests = line.split(": ")
        game_id = int(game_info.split(" ")[1])

        for single_test_substring in tests.split("; "):
            single_test_parsed = {}
            for cube_test in single_test_substring.split(", "):
                count, color = cube_test.split(" ")
                single_test_parsed[color] = int(count)
            result[game_id].append(single_test_parsed)
    
    return result    