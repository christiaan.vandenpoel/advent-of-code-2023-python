# Advent of Code - Day 2 - Part One

from aoc.day02.game import parse_game_info

def result(lines):
    result = 0
    set = {
        "red": 12,
        "green": 13,
        "blue": 14
    }

    game_info = parse_game_info(lines)

    for game_id, tests in game_info.items():
        possible = all(count <= set[color] for test in tests for color,count in test.items())
        if possible:
            result += game_id

    return result
