# Advent of Code - Day 19 - Part One
def create_rule_def(rule_definition):
    if '<' in rule_definition:
        condition, destination = rule_definition.split(":")
        left,right = condition.split("<")
        return (lambda p: destination if p[left] < int(right) else None)
    elif '>' in rule_definition:
        condition, destination = rule_definition.split(":")
        left,right = condition.split(">")
        return (lambda p: destination if p[left] > int(right) else None)
    else:
        return lambda _: rule_definition

def result(lines):
    rules_input, parts_input = "\n".join(lines).split("\n\n")

    rules = dict()
    parts = list()

    for rule_input in rules_input.split("\n"):
        rule_name, rule_definitions = rule_input.split("{")
        rule_list = [create_rule_def(rd) for rd in rule_definitions[:-1].split(",")]
        rules[rule_name] = rule_list

    for line in parts_input.split("\n"):
        part_definition = dict()
        for part_def in line.replace("{","").replace("}", "").split(","):
            left, right = part_def.split("=")
            part_definition[left] = int(right)
        parts.append(part_definition)

    accepted = list()
    for part in parts:
        position = 'in'
        while position not in ['A', 'R']:
            for r in rules[position]:
                if (next_pos := r(part)) is not None:
                    position = next_pos
                    break
        if position == 'A':
            accepted.append(part)    

    return sum([subtotal for acc in accepted for subtotal in acc.values()])