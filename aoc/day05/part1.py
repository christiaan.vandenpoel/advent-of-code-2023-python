# Advent of Code - Day 5 - Part One
from aoc.day05.helper import parse_almanac, find_location

def result(lines):
    seeds, almanac = parse_almanac(lines)

    values = find_location(almanac, seeds)

    return min(values)
