class Convertor():
    def __init__(self, destination_start, source_range_start, range_length) -> None:
        self.destination_start = destination_start
        self.source_range_start = source_range_start
        self.range_length = range_length

    def in_source_range(self, source):
        return self.source_range_start <= source and source < (self.source_range_start + self.range_length)
    
    def convert_range(self, source):
        if not self.in_source_range(source):
            return source
        diff = source - self.source_range_start
        return self.destination_start + diff

def parse_almanac(lines):
    parts = "\n".join(lines).split("\n\n")

    seedlist = parts[0]
    seeds = list(map(lambda i: int(i), seedlist.replace("seeds: ","").split(" ")))

    almanac = dict()

    for mapdefinition in parts[1:]:
        mapdefinition = mapdefinition.split("\n")
        source, destination = mapdefinition[0].replace("to-","").replace(" map:", "").split("-")

        convertors=list()
        for convertor_definition in mapdefinition[1:]:
            destination_range_start, source_range_start, range_length = list(map(lambda c: int(c), convertor_definition.split(" ")))
            convertors.append(Convertor(destination_range_start, source_range_start, range_length))
        
        almanac[source] = (destination, convertors)
    return seeds, almanac

def find_location(almanac, values):
    category = "seed"
    while category != "location":
        next_category, convertors = almanac[category]
        result = list()
        for value in values:
            in_range_of_convertors = False
            for convertor in convertors:
                if convertor.in_source_range(value):
                    in_range_of_convertors = True
                    result.append(convertor.convert_range(value))
            if not in_range_of_convertors:
                result.append(value)

        values = result
        category = next_category

    return values