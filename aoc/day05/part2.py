# Advent of Code - Day 5 - Part Two
from aoc.day05.helper import parse_almanac, find_location

def result(lines):
    print("!!! Running this takes a long time!")
    seeds, almanac = parse_almanac(lines)

    new_list = list()
    for idx in range(0, len(seeds), 2):
        new_list += (list(range(seeds[idx],seeds[idx]+seeds[idx+1])))

    values = find_location(almanac, new_list)

    return min(values)
