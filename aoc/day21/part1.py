# Advent of Code - Day 21 - Part One

def result(lines,steps):
    # print("Skipping! This take too much time/memory for 64 steps")
    # return 0
    start = None
    chart = dict()
    directions = [(-1,0),(0,-1),(1,0),(0,1)]
    width = len(lines[0])
    height = len(lines)
    
    for y, line in enumerate(lines):
        for x, char in enumerate(line):
            if char == 'S':
                start = (x,y)
                chart[(x,y)] = '.'
                continue
            chart[(x,y)] = char

    tracks = set([start])

    while steps > 0:
        print("steps: {0}, nr of tracks: {1}".format(steps, len(tracks)))
        steps -= 1
        new_tracks = set()

        for track in tracks:
            x,y = track

            for (dx,dy) in directions:
                new_pos = (x+dx,y+dy)
                if chart[new_pos] == '.':
                    new_tracks.add(new_pos)

        tracks = new_tracks

    return (len(tracks))

