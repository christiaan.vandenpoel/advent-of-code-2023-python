# Advent of Code - Day 9 - Part One

def result(lines):
    result = 0

    for line in lines:
        ints = [int(x) for x in line.split(" ")]

        ints_and_differences = [ints]
        while not all([x == 0 for x in ints_and_differences[-1]]):
            last_list = ints_and_differences[-1]

            new_list = []
            for idx in range(0, len(last_list)-1):
                new_list.append(last_list[idx+1] - last_list[idx])
            
            ints_and_differences.append(new_list)

        ints_and_differences.reverse()

        for idx, diff in enumerate(ints_and_differences):
            if idx == 0:
                diff.append(0)
            else:
                diff.append(diff[-1]+ints_and_differences[idx-1][-1])

        result += ints_and_differences[-1][-1]

    return result
