from collections import deque


def parse_chart(lines):
    chart = dict()

    for y, line in enumerate(lines):
        for x, char in enumerate(line):
            chart[(x,y)] = char

    return chart

def next_position(coordinate, direction):
    x,y = coordinate

    if direction == 'north':
        return (x, y-1)
    elif direction == 'east':
        return (x + 1, y)
    elif direction == 'south':
        return (x, y + 1)
    elif direction == 'west':
        return (x - 1, y)
    else:
        raise NotImplementedError("Unknown direction '{0}'".format(direction))

def calculate_total_energized(chart, start_location, start_direction):
    energized_locations = set()
    visited_locations_and_directions = set()
    beams = deque([
        (start_location, start_direction)
    ])

    while beams:
        beam = beams.pop()
        loc, direction = beam

        if loc not in chart:
            continue

        energized_locations.add(loc)
        if (beam) in visited_locations_and_directions:
            continue;
        visited_locations_and_directions.add(beam)

        if chart[loc] == '.':
            beams.append((next_position(loc, direction), direction))
        elif chart[loc] == '|':
            if direction in ['north','south']:
                next_loc = next_position(loc, direction)
                beams.append((next_loc,direction))
            elif direction == 'east':
                beams.append((next_position(loc, 'north'), 'north'))
                beams.append((next_position(loc, 'south'), 'south'))
            elif direction == 'west':
                beams.append((next_position(loc, 'north'), 'north'))
                beams.append((next_position(loc, 'south'), 'south'))
            else:
                raise NotImplementedError("unsupported mirror '{0}' for direction '{1}'".format(chart[loc],direction))
        elif chart[loc] == '-':
            if direction in ['east','west']:
                next_loc = next_position(loc, direction)
                beams.append((next_loc,direction))
            elif direction == 'south':
                beams.append((next_position(loc, 'east'), 'east'))
                beams.append((next_position(loc, 'west'), 'west'))
            elif direction == 'north':
                beams.append((next_position(loc, 'east'), 'east'))
                beams.append((next_position(loc, 'west'), 'west'))
            else:
                raise NotImplementedError("unsupported mirror '{0}' for direction '{1}'".format(chart[loc],direction))
        elif chart[loc] == '/':
            if direction == 'east':
                beams.append((next_position(loc, 'north'), 'north'))
            elif direction == 'north':
                beams.append((next_position(loc, 'east'), 'east'))
            elif direction == 'west':
                beams.append((next_position(loc, 'south'), 'south'))
            elif direction == 'south':
                beams.append((next_position(loc, 'west'), 'west'))
            else:
                raise NotImplementedError("unsupported mirror '{0}' for direction '{1}'".format(chart[loc],direction))
        elif chart[loc] == '\\':
            if direction == 'north':
                beams.append((next_position(loc, 'west'), 'west'))
            elif direction == 'east':
                beams.append((next_position(loc, 'south'), 'south'))
            elif direction == 'west':
                beams.append((next_position(loc, 'north'), 'north'))
            elif direction == 'south':
                beams.append((next_position(loc, 'east'), 'east'))
            else:
                raise NotImplementedError("unsupported mirror '{0}' for direction '{1}'".format(chart[loc],direction))
        else:
            raise NotImplementedError("unsupported value: '{0}' direction: {1}".format(chart[loc], direction))

    return len(energized_locations)
