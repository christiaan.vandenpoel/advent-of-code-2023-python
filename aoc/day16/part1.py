# Advent of Code - Day 16 - Part One
from aoc.day16.utils import calculate_total_energized, parse_chart

def result(lines):
    chart = parse_chart(lines)
    return calculate_total_energized(chart, (0,0), 'east')
