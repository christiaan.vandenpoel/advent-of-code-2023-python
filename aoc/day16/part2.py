# Advent of Code - Day 16 - Part Two
from aoc.day16.utils import calculate_total_energized, parse_chart

def result(lines):
    chart = parse_chart(lines)
    width = len(lines[0])
    height = len(lines)

    print("width: {0} x height: {1}".format(width, height))

    energized_total = 0

    # east: x = 0, y = 0..height-1
    for y in range(height):
        energized_total = max(energized_total,calculate_total_energized(chart,(0,y), 'east'))

    # south
    for x in range(width):
        energized_total = max(energized_total,calculate_total_energized(chart,(x,0), 'south'))

    # west
    for y in range(height):
        energized_total = max(energized_total,calculate_total_energized(chart,(width-1,y), 'west'))

    # north
    for x in range(width):
        energized_total = max(energized_total,calculate_total_energized(chart,(x,height-1), 'north'))

    return energized_total

