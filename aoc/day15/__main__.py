#!/usr/bin/env python3

from pathlib import Path
import time

from aoc.day15 import part1, part2

def read_file(filename):
    path = Path(__file__).parent.resolve()
    with open(path / filename, 'r') as f:
        lines = f.read().splitlines()
        return lines

def main():
    input = read_file("./resources/input.txt")

    print("--- Part One ---")
    start = time.time()
    result = part1.result(input)
    end = time.time()
    print("Result: {0} ({1:.03f} sec)".format(result, end - start))

    print("--- Part Two ---")
    start = time.time()
    result = part2.result(input)
    end = time.time()
    print("Result: {0} ({1:.03f} sec)".format(result, end - start))
    
if __name__ == "__main__":
    main()
