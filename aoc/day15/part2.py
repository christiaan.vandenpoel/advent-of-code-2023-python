# Advent of Code - Day 15 - Part Two
import re
from aoc.day15.utils import hash
from collections import defaultdict


def remove_lens_with_label_from_box(boxes, label, boxnr):
    lenses = boxes[boxnr]
    boxes[boxnr] = [(lbl, nr) for (lbl, nr) in lenses if lbl != label]


def add_lens_nr_with_label_to_box(boxes, label, boxnr, lensnr):
    lenses = boxes[boxnr]
    new_lenses = []
    boxes[boxnr] = new_lenses
    replaced = False
    for lbl_and_lens in lenses:
        lbl, lens = lbl_and_lens
        if lbl == label:
            replaced = True
            new_lenses.append((lbl, lensnr))
        else:
            new_lenses.append((lbl, lens))

    if not replaced:
        new_lenses.append((label, lensnr))


def result(lines):
    total = 0
    boxes = defaultdict(list)

    for string in lines[0].split(","):
        match = re.match("([^-=]*)([=-])(\d*)?", string)
        label = match.groups()[0]
        boxnr = hash(label)

        operation = match.groups()[1]

        if operation == '-':
            remove_lens_with_label_from_box(boxes, label, boxnr)
        elif operation == '=':
            lensnr = int(match.groups()[2])
            add_lens_nr_with_label_to_box(boxes, label, boxnr, lensnr)

    for boxnr, lenses in boxes.items():
        for idx, lens in enumerate(lenses):
            v = (boxnr + 1) * (idx + 1) * lens[1]
            total += v

    return total
