# Advent of Code - Day 15 - Part One
from aoc.day15.utils import hash


def result(lines):

    total = 0

    for string in lines[0].split(","):
        total += hash(string)

    return total
