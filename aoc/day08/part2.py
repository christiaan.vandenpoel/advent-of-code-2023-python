# Advent of Code - Day 8 - Part Two
from aoc.day08.utils import parse_input
from itertools import cycle
import math

def result(input):
    steps, nodes = parse_input(input)

    step_count = 0
    current_positions = [node for node in nodes.keys() if node[2] == 'A']

    steps_taken_per_ghost = []

    for current_position in current_positions:
        step_count = 0
        for step in cycle(steps):
            step_count += 1
            if step == 'L':
                current_position = (nodes[current_position][0])
            else:
                current_position = (nodes[current_position][1])

            if current_position.endswith('Z'):
                steps_taken_per_ghost.append(step_count)
                break

    return math.lcm(*steps_taken_per_ghost)