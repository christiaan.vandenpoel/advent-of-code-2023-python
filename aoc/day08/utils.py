
def parse_input(lines):
    steps = lines[0]
    nodes = dict()

    # 'AAA = (BBB, CCC)'
    for line in lines[2:]:
        source,destinations=line.split(" = ")

        nodes[source] = destinations.replace("(","").replace(")","").split(", ")

    return steps, nodes
