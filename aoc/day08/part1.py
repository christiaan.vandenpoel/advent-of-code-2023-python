# Advent of Code - Day 8 - Part One
from aoc.day08.utils import parse_input
from itertools import cycle

def result(input):
    steps, nodes = parse_input(input)

    step_count = 0
    current_position = 'AAA'
    for step in cycle(steps):
        step_count += 1
        if step == 'L':
            current_position = nodes[current_position][0]
        else:
            current_position = nodes[current_position][1]

        if current_position == 'ZZZ':
            break

    return step_count
