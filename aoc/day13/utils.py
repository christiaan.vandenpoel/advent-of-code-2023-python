def find_vertical_reflection_line(pattern, orig_value = None):
    nr_of_cols = len(pattern[0])
    for col_idx in range(1, nr_of_cols):
        left_indices = list(range(col_idx-1,-1,-1))
        right_indices = list(range(col_idx, nr_of_cols))

        subsize = min(len(left_indices), len(right_indices))

        mirror = True
        for subindex in range(0,subsize):
            mirror &= all([line[col_idx-subindex-1] == line[col_idx+subindex] for line in pattern])
        
        if mirror:
            if orig_value is not None:
                if orig_value != col_idx:
                    return col_idx
            else:
                return col_idx

    return 0

def find_horizontal_reflection_line(pattern, orig_value = None):
    nr_or_rows = len(pattern)

    for row_idx in range(1,len(pattern)):
        left_indices = list(range(row_idx-1,-1,-1))
        right_indices = list(range(row_idx, nr_or_rows))

        subsize = min(len(left_indices), len(right_indices))

        mirror = True
        for subindex in range(0,subsize):
            mirror &= pattern[row_idx-subindex-1] == pattern[row_idx+subindex]
        
        if mirror:
            if orig_value is not None:
                if orig_value != (row_idx) * 100:
                    return (row_idx) * 100
            else:
                return (row_idx) * 100

    return 0