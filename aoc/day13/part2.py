# Advent of Code - Day 13 - Part Two
from aoc.day13.utils import find_horizontal_reflection_line, find_vertical_reflection_line

def iterate_pattern(pattern):
    nr_of_elements = len(pattern) * len(pattern[0])
    for offset in range(0,nr_of_elements):
        new_pattern = []
        for y, line in enumerate(pattern):
            new_pattern.append([])
            for x, char in enumerate(line):
                current_offset = (y)*len(pattern[0]) + x
                if current_offset == offset:
                    if char == '.':
                        new_pattern[-1].append('#')
                    else:
                        new_pattern[-1].append('.')
                else:
                    new_pattern[-1].append(char)

        yield new_pattern

def result(lines):
    patterns = [pattern.splitlines() for pattern in "\n".join(lines).split("\n\n")]

    result = 0

    for pattern in patterns:
        v1_orig = find_vertical_reflection_line(pattern)
        v2_orig = find_horizontal_reflection_line(pattern)
        for iterated_pattern in iterate_pattern(pattern):
            v1 = find_vertical_reflection_line(iterated_pattern, v1_orig)
            if v1 > 0 and v1 != v1_orig:
                result += v1
                break
            v2 = find_horizontal_reflection_line(iterated_pattern, v2_orig)
            if v2 > 0 and v2 != v2_orig:
                result += v2
                break

    return result