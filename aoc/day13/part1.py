# Advent of Code - Day 13 - Part One
from aoc.day13.utils import find_horizontal_reflection_line, find_vertical_reflection_line

def result(lines):
    patterns = [pattern.splitlines() for pattern in "\n".join(lines).split("\n\n")]

    result = 0

    for pattern in patterns:
        v1 = find_vertical_reflection_line(pattern)
        v2 = find_horizontal_reflection_line(pattern)
        result += (v1 + v2)

    return result
    