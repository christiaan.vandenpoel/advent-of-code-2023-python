# Advent of Code - Day 17 - Part One
import heapq

def next_steps(chart, location, direction, forward_steps):
    new_loc, new_direction, new_forward_steps = None, None, None
    (x,y) = location

    east_loc = (x+1,y)
    south_loc = (x,y+1)
    north_loc = (x,y-1)
    west_loc = (x-1, y)

    if direction == 'east':
        if east_loc in chart:
            if forward_steps > 0:
                yield(east_loc, 'east', forward_steps-1)
        if north_loc in chart:
            yield(north_loc, 'north', 3)
        if south_loc in chart:
            yield(south_loc, 'south', 3)
    elif direction == 'west':
        if west_loc in chart:
            if forward_steps > 0:
                yield(west_loc, 'west', forward_steps-1)
        if north_loc in chart:
            yield(north_loc, 'north', 3)
        if south_loc in chart:
            yield(south_loc, 'south', 3)
    elif direction == 'south':
        if south_loc in chart:
            if forward_steps > 0:
                yield(south_loc, 'south', forward_steps-1)
        if west_loc in chart:
            yield(west_loc, 'west', 3)
        if east_loc in chart:
            yield(east_loc, 'east', 3)
    elif direction == 'north':
        if north_loc in chart:
            if forward_steps > 0:
                yield(north_loc, 'north', forward_steps-1)
        if west_loc in chart:
            yield(west_loc, 'west', 3)
        if east_loc in chart:
            yield(east_loc, 'east', 3)
    else:
        raise NotImplementedError("not implemented: {0}".format(direction))            

    return new_loc, new_direction, new_forward_steps

# 669: too low
# 686: ok
def result(lines):
    chart = dict()

    height = len(lines)
    width = len(lines[0])
    chart = {(x,y): int(lines[y][x]) for y in range(height) for x in range(width)}

    source = (0,0)
    destination = (width-1,height-1)
    visited = set()
    queue = [
        (0, source, 'east', [], 3),
        (0, source, 'south', [], 3),
    ]
    heapq.heapify(queue)
    result = None
    # count = 2000000000000
    while queue: # and count > 0:
        # print(" === processing next path ===")
        # print(" ---       queue          ---")
        # for item in queue:
        #     print(item)
        # print(" ---       queue          ---")
        # count -= 1
        (cost, location, direction, path, forward_steps) = heapq.heappop(queue)

        # print("going '{0}' to location {1} ({2})".format(direction, location, forward_steps))
        new_cost = cost + chart[location]
        # print("  cost: {}".format(new_cost))
        # print("  path: {0}".format(path))

        if location in visited:
            # print("  skipping because visited earlier")  
            continue      
        visited.add(location)

        if location == destination:
            # print(path)
            # for p in path:
            #     print("chart[{0}] = {1}".format(p, chart[p]))
            if result is None:
                result = new_cost - chart[source]
            else:
                result = min(result, new_cost - chart[source])
            continue
        else:
            # print("  not the end, so keep on working")
            pass

        # now add new next locations
        # keeping in mind the regulations
        for next_step in next_steps(chart, location, direction, forward_steps):
            new_loc, new_direction, new_forward_steps = next_step
            # print("  >> candidate: loc: {0}, direction: {1}, steps: {2} ({3})"
            #       .format(new_loc, new_direction, new_forward_steps, path))
            if new_loc in path: # or new_loc in visited:
                # print("  !! skipping because already visited")
                continue
            new_path = path.copy()
            new_path.append(location)
            heapq.heappush(queue, (new_cost, new_loc, new_direction, new_path, new_forward_steps))

    return result
