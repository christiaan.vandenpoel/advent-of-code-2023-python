# Advent of Code - Day 17 - Part One
import heapq
from heapq import heapify, heappop, heappush

def find_end(a, b, c, grid, adj, end):
    heapify(queue := [(0, 1, 0, (0, 0))])
    best = {}
    while queue:
        loss, chain, direction, current = heappop(queue)
        if (key := (current, direction, chain)) in best and best[key] <= loss: 
            continue
        if current == end and chain >= a: 
            print(chain)
            return loss
        best[key] = loss
        neighbours = adj(*current)
        for e, d in enumerate([(direction - 1) % 4, direction, (direction + 1) % 4]):
            if [chain < b, chain == c][e % 2]: 
                continue
            next_chain = [1, chain + 1][e % 2]
            if (neighbour := neighbours[d]) in grid and best.get((neighbour, d, next_chain), loss + 1) > loss:
                heappush(queue, (loss + grid[neighbour], next_chain, d, neighbour))


def result(lines):
    grid = {(x, y) : int(lines[y][x]) for x in range(len(lines[0])) for y in range(len(lines))}
    end = (len(lines[0]) - 1, len(lines) - 1)
    adj = lambda x, y: ((x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1))
    print(find_end(0, 0, 3, grid, adj, end), find_end(4, 4, 10, grid, adj, end))
    return find_end(0, 0, 3, grid, adj, end)