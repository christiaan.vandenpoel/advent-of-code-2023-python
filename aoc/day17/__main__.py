#!/usr/bin/env python3

from pathlib import Path
import time

from aoc.day17 import part1, part2, part1_example

def read_file(filename):
    path = Path(__file__).parent.resolve()
    with open(path / filename, 'r') as f:
        lines = f.read().splitlines()
        return lines

def main():
    input = read_file("./resources/input.txt")

    print("--- Part One ---")
    start = time.time()
    # print(part1_example.result(input))
    result = part1.result(input)
    end = time.time()
    print("Result: {0} ({1:.03f} sec)".format(result, end - start))

    print("--- Part Two ---")
    start = time.time()
    result = part2.result(input)
    end = time.time()
    print("Result: {0} ({1:.03f} sec)".format(result, end - start))
    
if __name__ == "__main__":
    main()
