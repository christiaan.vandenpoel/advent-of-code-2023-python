def parse_galaxies(lines, increase = 1):
    if increase > 1:
        increase -= 1

    unique_x = set()
    unique_y = set()
    max_x = len(lines[0]) - 1
    max_y = len(lines) - 1
    
    galaxies = list()

    for y, line in enumerate(lines):
        for x, char in enumerate(line):
            if char == '#':
                unique_x.add(x)
                unique_y.add(y)
                galaxies.append((x,y))

    for x in range(max_x, -1, -1):
        if x not in unique_x:
            new_galaxies = list()
            for galaxy in galaxies:
                x1,y1 = galaxy
                if x1 < x:
                    new_galaxies.append(galaxy)
                else:
                    new_galaxies.append((x1+increase, y1))
            galaxies = new_galaxies

    for y in range(max_y, -1, -1):
        if y not in unique_y:
            new_galaxies = list()
            for galaxy in galaxies:
                x1,y1 = galaxy
                if y1 < y:
                    new_galaxies.append(galaxy)
                else:
                    new_galaxies.append((x1, y1+increase))
            galaxies = new_galaxies

    return galaxies