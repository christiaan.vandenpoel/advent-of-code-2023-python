# Advent of Code - Day 11 - Part Two
from itertools import combinations
from aoc.day11.utils import parse_galaxies

def result(lines, increase):
    galaxies = parse_galaxies(lines, increase)

    result = 0
    for pair in combinations(galaxies,2):
        x1,y1 = pair[0]
        x2,y2 = pair[1]
        distance = abs(x2-x1)+abs(y2-y1)
        result += distance
    return result
