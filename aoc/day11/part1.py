# Advent of Code - Day 11 - Part One
from itertools import combinations
from aoc.day11.utils import parse_galaxies

def result(lines):
    galaxies = parse_galaxies(lines)

    result = 0
    for pair in combinations(galaxies,2):
        x1,y1 = pair[0]
        x2,y2 = pair[1]
        distance = abs(x2-x1)+abs(y2-y1)
        result += distance
    return result
