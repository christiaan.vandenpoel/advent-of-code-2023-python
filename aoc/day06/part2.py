# Advent of Code - Day 6 - Part Two

def result(input):
    print("Takes 2 to 3 seconds....")
    result = 0

    duration = int(input[0].replace("Time:", "").strip().replace(" ", ""))
    distance = int(input[1].replace("Distance:", "").strip().replace(" ", ""))

    for d in range(duration,-1,-1):
        if (d * (duration-d)) > distance:
            result += 1

    return result