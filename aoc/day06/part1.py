# Advent of Code - Day 6 - Part One
import re

def result(input):
    result = 1

    durations = list(map(lambda x: int(x), re.split("\s+", input[0].replace("Time:", "").strip())))
    distances = list(map(lambda x: int(x), re.split("\s+", input[1].replace("Distance:", "").strip())))

    for idx, duration in enumerate(durations):
        distance = distances[idx]

        result *= len(list(filter(lambda d: d * (duration-d) > distance,range(duration,-1,-1))))

    return result
