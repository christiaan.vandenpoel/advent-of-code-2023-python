# Advent of Code - Day 7 - Part Two
from collections import Counter

class Bid():
    faces = ['J','2','3','4','5','6','7','8','9','T','Q','K','A']

    def __init__(self, hand, bid) -> None:
        self.hand = hand
        self.bid = bid
        self.type = self._type()
    
    def __str__(self):
        return "Bid(hand:'{0}',bid:{1},type:{2})".format(self.hand, self.bid, self.type)

    def __repr__(self):
        return self.__str__()
    
    def _type(self):
        # high_card: 0
        # one pair: 1
        # two pair: 2
        # three of a kind: 3
        # full house: 4
        # four of a kind: 5
        # five 0f a kind: 6     
           
        count = Counter(self.hand)
        first = count.most_common(1)[0]
        nr_of_jokers = 0
        if 'J' in count.keys():
            nr_of_jokers = count['J']

        if len(count) == 1:
            return 6
        first, second = count.most_common(2)
        if first[0] == 'J':
            first = second

        first_type = first[1] + nr_of_jokers
        if first_type == 5:
            return 6
        if first_type == 4:
            return 5
        
        second = count.most_common(2)[1]
        
        if first_type == 3 and second[1] == 2:
            return 4
        if first_type == 3 and second[1] == 1:
            return 3
        if first_type == 2 and second[1] == 2:
            return 2
        if first_type == 2 and second[1] == 1:
            return 1
        return 0

    def __lt__(self, other):
        # first by type (from lowest to highest)
        my_type = self.type
        other_type = other.type

        if my_type != other_type:
            return my_type < other_type
        
        for idx, face in enumerate(self.hand):
            other_face = other.hand[idx]

            my_value = self.faces.index(face)
            other_value = self.faces.index(other_face)

            if my_value != other_value:
                return my_value < other_value

        # if the same type, then by which card is the strongest
        # 2,3,4,5,6,7,8,9,T,J,Q,K,A
        raise Exception("not supported yet!!!")

def result(lines):
    bids = list()
    for line in lines:
        hand, bid = line.split(" ")
        bids.append(Bid(hand, int(bid)))

    bids.sort()

    result = 0
    for idx, bid in enumerate(bids):
        result += (idx+1) * bid.bid
    return result
