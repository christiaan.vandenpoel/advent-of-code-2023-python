# Advent of Code - Day 7 - Part One
from collections import Counter

class Bid():
    faces = ['2','3','4','5','6','7','8','9','T','J','Q','K','A']

    def __init__(self, hand, bid) -> None:
        self.hand = hand
        self.bid = bid
        self.type = self._type()
    
    def __str__(self):
        return "Bid(hand:'{0}',bid:{1},type:{2})".format(self.hand, self.bid, self.type)

    def __repr__(self):
        return self.__str__()
        return "Bid(hand:'{0}',bid:{1})".format(self.hand, self.bid)
    
    def _type(self):
        # high_card: 0
        # one pair: 1
        # two pair: 2
        # three of a kind: 3
        # full house: 4
        # four of a kind: 5
        # five 0f a kind: 6        
        count = Counter(self.hand)
        if len(count) == 1:
            return 6
        first, second = count.most_common(2)
        if first[1] == 4:
            return 5
        if first[1] == 3 and second[1] == 2:
            return 4
        if first[1] == 3 and second[1] == 1:
            return 3
        if first[1] == 2 and second[1] == 2:
            return 2
        if first[1] == 2 and second[1] == 1:
            return 1
        return 0

    def __lt__(self, other):
        # first by type (from lowest to highest)
        my_type = self.type
        other_type = other.type

        if my_type != other_type:
            return my_type < other_type
        
        for idx, face in enumerate(self.hand):
            other_face = other.hand[idx]

            my_value = self.faces.index(face)
            other_value = self.faces.index(other_face)

            if my_value != other_value:
                return my_value < other_value

        # if the same type, then by which card is the strongest
        # 2,3,4,5,6,7,8,9,T,J,Q,K,A
        raise Exception("not supported yet!!!")


def result(lines):
    bids = list()
    for line in lines:
        hand, bid = line.split(" ")
        bids.append(Bid(hand, int(bid)))

    bids.sort()

    result = 0
    for idx, bid in enumerate(bids):
        result += (idx+1) * bid.bid
    return result
