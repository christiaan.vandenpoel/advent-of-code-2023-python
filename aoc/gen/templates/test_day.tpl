from aoc.$module_name import part1, part2

example_input = """""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == None

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input) == None
