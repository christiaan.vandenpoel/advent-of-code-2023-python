# Advent of Code - Day 18 - Part Two
import re
from collections import deque

def result(lines):
    # R 6 (#70c710)
    hex_to_direction = "RDLU"
    steps = list()
    for line in lines:
        _, _, color = re.match("(\\w)\\s(\\d*)\\s\\(#([0-9a-f]*)\\)",line).groups()[0:3]
        direction = hex_to_direction[(int(color, 16) & 0x7)]
        step = int(color, 16) >> 4
        steps.append((direction, int(step)))

    chart = list()
    start = (0,0)
    chart.append(start)
    directions = { 'R': (1,0), 'U': (0,1), 'D': (0,-1), 'L': (-1,0)}
    current=start
    perimeter = 0
    for (direction, count, ) in steps:
        dx,dy = directions[direction]
        perimeter += count
        for _ in range(count):
            x,y = current
            x += dx
            y += dy
            current = (x,y)
            chart.append(current)

    area = 0
    for idx in range(len(chart)-1):
        area += chart[idx][0] * chart[idx+1][1] - chart[idx+1][0] * chart[idx][1]

    return abs(area)/2 + perimeter/2 + 1