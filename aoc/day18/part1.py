# Advent of Code - Day 18 - Part One
import re
from collections import deque

# 51280: too high
def result(lines):
    # R 6 (#70c710)
    steps = list()
    for line in lines:
        direction, step, color = re.match("(\\w)\\s(\\d*)\\s\\(#([0-9a-f]*)\\)",line).groups()[0:3]
        steps.append((direction, int(step), color))

    chart = list()
    start = (0,0)
    chart.append(start)
    directions = { 'R': (1,0), 'U': (0,1), 'D': (0,-1), 'L': (-1,0)}
    current=start
    perimeter = 0
    for (direction, count, color) in steps:
        dx,dy = directions[direction]
        perimeter += count
        for _ in range(count):
            x,y = current
            x += dx
            y += dy
            current = (x,y)
            
            chart.append(current)

    area = 0
    for idx in range(len(chart)-1):
        area += chart[idx][0] * chart[idx+1][1] - chart[idx+1][0] * chart[idx][1]

    return abs(area)/2 + perimeter/2 + 1

