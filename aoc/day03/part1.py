# Advent of Code - Day 3 - Part One
from aoc.day03.utils import list_adjacents, parse_chart


def check_for_valid_number(number, adjacent_to_symbol, total):
    if number != "":
        if adjacent_to_symbol:
            total += int(number)

        number = ""
        adjacent_to_symbol = False
    return number, adjacent_to_symbol, total


def result(lines):
    chart, symbols, max_x, max_y = parse_chart(lines)

    number = ""
    adjacent_to_symbol = False

    total = 0
    last_y = -1

    for coordinate in [(x, y) for y in range(max_y+1) for x in range(max_x+1)]:
        char = chart[coordinate]

        new_line_started = coordinate[1] != last_y
        char_is_a_symbol = char in symbols
        char_is_a_period = char == '.'

        if new_line_started or char_is_a_period or char_is_a_symbol:
            number, adjacent_to_symbol, total = check_for_valid_number(
                number, adjacent_to_symbol, total)
            last_y = coordinate[1]

        if char.isdigit():
            number += char
            adjacent_to_symbol |= any(
                chart[a] in symbols for a in list_adjacents(coordinate))

    return total
