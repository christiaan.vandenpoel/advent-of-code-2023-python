from collections import defaultdict

def list_adjacents(c):
    x,y = c
    for nx,ny in [(nx, ny) for nx in range(x-1,x+2) for ny in range(y-1,y+2)]:
        if nx == x and ny == y:
            continue
        yield (nx,ny)

def parse_chart(lines):
    chart = defaultdict(lambda: '.')
    max_x = 0
    max_y = 0

    symbols = set()
    
    for y, line in enumerate(lines):
        for x, char in enumerate(line):
            if not char.isdigit() and char != '.':
                symbols.add(char)
            chart[(x,y)] = char
            max_x = max(max_x, x)
            max_y = max(max_y, y)

    return chart, symbols, max_x, max_y