# Advent of Code - Day 3 - Part Two
from aoc.day03.utils import list_adjacents, parse_chart
from collections import defaultdict

def result(lines):
    chart, symbols, max_x, max_y = parse_chart(lines)

    gear_coordinates_to_numbers = defaultdict(list) 

    number = ""

    last_y = -1
    adjacent_gear_symbol_locations = set()

    for coordinate in [(x,y) for y in range(max_y+1) for x in range(max_x+1)]:
        char = chart[coordinate]

        new_line_started = coordinate[1] != last_y
        char_is_a_symbol = char in symbols
        char_is_a_period = char == '.'

        if new_line_started or char_is_a_symbol or char_is_a_period:
            if number != "":
                if len(adjacent_gear_symbol_locations) > 0:
                    for gear_location in adjacent_gear_symbol_locations:
                        gear_coordinates_to_numbers[gear_location].append(int(number))

                number = ""
                adjacent_gear_symbol_locations = set()
            last_y = coordinate[1]

        if char.isdigit():
            number += char

            for adj_coordinate in list_adjacents(coordinate):
                if chart[adj_coordinate] == '*':
                    adjacent_gear_symbol_locations.add(adj_coordinate)

    total = 0
    for _, numbers in gear_coordinates_to_numbers.items():
        if len(numbers) != 2:
            continue
        total += numbers[0]*numbers[1]

    return total
