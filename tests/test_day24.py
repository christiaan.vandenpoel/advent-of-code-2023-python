from aoc.day24 import part1, part2
import pytest

example_input = """19, 13, 30 @ -2,  1, -2
18, 19, 22 @ -1, -1, -2
20, 25, 34 @ -2, -2, -4
12, 31, 28 @ -1, -2, -1
20, 19, 15 @  1, -5, -3""".splitlines()

#
# --- Part One ---
#
@pytest.mark.skip("WIP")
def test_part1():
    assert part1.result(example_input, 6, 27) == 2

#
# --- Part Two ---
#
@pytest.mark.skip("WIP")
def test_part2():
    assert part2.result(example_input) == None
