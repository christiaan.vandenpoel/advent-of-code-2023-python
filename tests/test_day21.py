from aoc.day21 import part1, part2
import pytest

example_input = """...........
.....###.#.
.###.##..#.
..#.#...#..
....#.#....
.##..S####.
.##..#...#.
.......##..
.##.#.####.
.##..##.##.
...........""".splitlines()

#
# --- Part One ---
#
@pytest.mark.skip()
def test_part1():
    assert part1.result(example_input, 6) == 16

#
# --- Part Two ---
#

@pytest.mark.skip("WIP")
def test_part2():
    assert part2.result(example_input, 6) == 16
    assert part2.result(example_input, 10) == 50
    assert part2.result(example_input, 50) == 1594
    assert part2.result(example_input, 100) == 6536
    assert part2.result(example_input, 500) == 167004
    assert part2.result(example_input, 1000) == 668697
    assert part2.result(example_input, 5000) == 16733044

