from aoc.day22 import part1, part2
import pytest

example_input = """1,0,1~1,2,1
0,0,2~2,0,2
0,2,3~2,2,3
0,0,4~0,2,4
2,0,5~2,2,5
0,1,6~2,1,6
1,1,8~1,1,9""".splitlines()

#
# --- Part One ---
#
@pytest.mark.skip()
def test_part1():
    assert part1.result(example_input) == 5

#
# --- Part Two ---
#
@pytest.mark.skip()
def test_part2():
    assert part2.result(example_input) == None
