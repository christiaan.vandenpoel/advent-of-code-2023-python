from aoc.day07 import part1, part2
import pytest

example_input = """32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 6440

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input) == 5905
