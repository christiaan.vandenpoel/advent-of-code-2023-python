from aoc.day14 import part1, part2
import pytest

example_input = """O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....""".splitlines()

#
# --- Part One ---
#
def test_part1():
    assert part1.result(example_input) == 136

#
# --- Part Two ---
#
# @pytest.mark.skip("WIP")
def test_part2():
    assert part2.result(example_input) == 64
