from aoc.day09 import part1, part2

example_input = """0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 114

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input) == 2
