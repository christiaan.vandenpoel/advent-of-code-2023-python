from aoc.day15 import part1, part2
import pytest

example_input = """rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7""".splitlines()

#
# --- Part One ---
#
def test_part1():
    assert part1.result(example_input) == 1320

#
# --- Part Two ---
#
def test_part2():
    assert part2.result(example_input) == 145
