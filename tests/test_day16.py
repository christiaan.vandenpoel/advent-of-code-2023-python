from aoc.day16 import part1, part2
import pytest

example_input = """.|...\\....
|.-.\\.....
.....|-...
........|.
..........
.........\\
..../.\\\\..
.-.-/..|..
.|....-|.\\
..//.|....""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 46

#
# --- Part Two ---
#
def test_part2():
    assert part2.result(example_input) == 51
