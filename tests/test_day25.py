from aoc.day25 import part1, part2
import pytest

example_input = """jqt: rhn xhk nvd
rsh: frs pzl lsr
xhk: hfx
cmg: qnr nvd lhk bvb
rhn: xhk bvb hfx
bvb: xhk hfx
pzl: lsr hfx nvd
qnr: nvd
ntq: jqt hfx bvb xhk
nvd: lhk
lsr: lhk
rzs: qnr cmg lsr rsh
frs: qnr lhk lsr""".splitlines()

#
# --- Part One ---
#
@pytest.mark.skip("wip")
def test_part1():
    assert part1.result(example_input) == 54

#
# --- Part Two ---
#
@pytest.mark.skip("wip")
def test_part2():
    assert part2.result(example_input) == None
