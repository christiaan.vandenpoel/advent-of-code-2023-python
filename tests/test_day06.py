from aoc.day06 import part1, part2
import pytest

example_input = """Time:      7  15   30
Distance:  9  40  200""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 288

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input) == 71503
