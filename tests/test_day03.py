from aoc.day03 import part1, part2
import pytest

example_input = """467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 4361

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(example_input) == 467835
