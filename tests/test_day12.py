from aoc.day12 import part1, part2
import pytest

example_input = """???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1""".splitlines()

#
# --- Part One ---
#
@pytest.mark.skip("WIP")
def test_part1():
    assert part1.result(example_input) == 21

#
# --- Part Two ---
#
@pytest.mark.skip("WIP")
def test_part2():
    assert part2.result(example_input) == None
