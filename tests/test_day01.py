from aoc.day01 import part1, part2

example_input = """1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet""".splitlines()

second_eaxmple = """two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 142

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(second_eaxmple) == 281
