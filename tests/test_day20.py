from aoc.day20 import part1, part2
import pytest

example_input = """broadcaster -> a, b, c
%a -> b
%b -> c
%c -> inv
&inv -> a""".splitlines()

second_example_input = """broadcaster -> a
%a -> inv, con
&inv -> b
%b -> con
&con -> output""".splitlines()

#
# --- Part One ---
#
@pytest.mark.skip()
def test_part1():
    assert part1.result(example_input) == 32000000


@pytest.mark.skip()
def test_part1_second_example():
    assert part1.result(second_example_input) == 11687500

#
# --- Part Two ---
#
@pytest.mark.skip()
def test_part2():
    assert part2.result(example_input) == None
