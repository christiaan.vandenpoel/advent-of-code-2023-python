from aoc.day10 import part1, part2
import pytest

example_input = """.....
.S-7.
.|.|.
.L-J.
.....""".splitlines()

second_example_input = """..F7.
.FJ|.
SJ.L7
|F--J
LJ...""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 4

def test_part1_second_example():
    assert part1.result(second_example_input) == 8

part2_first_example = """...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........""".splitlines()

part2_second_example = """.F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...""".splitlines()

part2_third_example = """FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L""".splitlines()

#
# --- Part Two ---
#
@pytest.mark.skip("Much to difficult")
def test_part2():
    assert part2.result(part2_first_example) == 4

@pytest.mark.skip("Much to difficult")
def test_part2_second_example():
    assert part2.result(second_example_input) == 8

@pytest.mark.skip("Much to difficult")
def test_part2_third_example():
    assert part2.result(part2_first_example) == 10
