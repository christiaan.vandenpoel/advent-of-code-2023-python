This repository is a collection of my solutions for the [Advent of Code 2023](http://adventofcode.com/2023) calendar.

Puzzle                                                    |
--------------------------------------------------------- |
[Day 1: ⭐️⭐️  Trebuchet?!](aoc/day01)                      |  
[Day 2: ⭐️⭐️  Cube Conundrum](aoc/day02)                   |
[Day 3: ⭐️⭐️  Gear Ratios](aoc/day03)           |
[Day 4: ⭐️⭐️  Scratchcards ](aoc/day04)                      |
[Day 5: ⭐️    If You Give A Seed A Fertilizer](aoc/day05)                     |
[Day 6: ⭐️⭐️  Wait For It](aoc/day06)                    |
[Day 7: ⭐️⭐️  Camel Cards](aoc/day07)           |   
[Day 8: ⭐️⭐️  Haunted Wasteland](aoc/day08)                |
[Day 9: ⭐️⭐️  Mirage Maintenance](aoc/day09)                       |
[Day 10: ⭐️ Pipe Maze ](aoc/day10)                 |
[Day 11: ⭐️⭐️ Cosmic Expansion](aoc/day11)             |
[Day 12: Hot Springs ](aoc/day12)          |
[Day 13: ⭐️⭐️ Point of Incidence](aoc/day13)                  |
[Day 14: ⭐️⭐️ Parabolic Reflector Dish](aoc/day14)               |
[Day 15: ⭐️⭐️ Lens Library](aoc/day15)            |
[Day 16: ⭐️⭐️ The Floor Will Be Lava](aoc/day16)            |
[Day 17: Clumsy Crucible](aoc/day17)                 |
[Day 18: ⭐️ Lavaduct Lagoon](aoc/day18)                 |
[Day 19: ⭐️ Aplenty](aoc/day19)              |
[Day 20: Pulse Propagation](aoc/day20)         |
[Day 21: ⭐️ Step Counter](aoc/day21)                      |
[Day 22: Sand Slabs](aoc/day22)                       |
[Day 23: ⭐️ A Long Walk](aoc/day23)               |
[Day 24:](aoc/day24)                   |
[Day 25:](aoc/day25)                  |
